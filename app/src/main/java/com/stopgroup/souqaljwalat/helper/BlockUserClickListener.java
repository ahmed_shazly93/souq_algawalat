package com.stopgroup.souqaljwalat.helper;

import com.stopgroup.souqaljwalat.modelsList.blockUserModel;

public interface BlockUserClickListener {
    void onItemClick(blockUserModel item, int position);

}
