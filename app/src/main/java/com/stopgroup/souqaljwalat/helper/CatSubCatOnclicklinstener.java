package com.stopgroup.souqaljwalat.helper;

import android.view.View;

import com.stopgroup.souqaljwalat.modelsList.catSubCatlistModel;

public interface CatSubCatOnclicklinstener {
    void onItemClick(catSubCatlistModel item);

    void onItemTouch(catSubCatlistModel item);

    void addToFavClick(View v, String position);

}
