package com.stopgroup.souqaljwalat.helper;

public interface ItemTouchHelperViewHolder {

    void onItemSelected();

    void onItemClear();
}
