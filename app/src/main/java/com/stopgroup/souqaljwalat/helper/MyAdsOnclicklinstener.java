package com.stopgroup.souqaljwalat.helper;

import android.view.View;

import com.stopgroup.souqaljwalat.modelsList.myAdsModel;

public interface MyAdsOnclicklinstener {

    void onItemClick(myAdsModel item);

    void delViewOnClick(View v, int position);

    void editViewOnClick(View v, int position);

}
