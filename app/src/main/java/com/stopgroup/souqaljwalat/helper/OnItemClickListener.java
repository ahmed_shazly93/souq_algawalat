package com.stopgroup.souqaljwalat.helper;

import com.stopgroup.souqaljwalat.modelsList.homeCatListModel;

public interface OnItemClickListener {
    void onItemClick(homeCatListModel item);
}
