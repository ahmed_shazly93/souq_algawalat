package com.stopgroup.souqaljwalat.home.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stopgroup.souqaljwalat.R;
import com.stopgroup.souqaljwalat.home.helper.CategoryModel;

import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CategoryModel> List_Item;
    private Context context;

    public CategoriesAdapter(List<CategoryModel> list_Item, Context context) {
        List_Item = list_Item;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.item_category, viewGroup, false);
        return new MenuItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final MenuItemViewHolder menuItemHolder = (MenuItemViewHolder) holder;
        final CategoryModel model = List_Item.get(position);
//        menuItemHolder.name.setText(model.getName());
//        menuItemHolder.name.setGravity(Gravity.CENTER);

        menuItemHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (List_Item.get(position).isHas_sub_category()) {
//                    context.startActivity(new Intent(context, SubCategiesActivity.class)
//                            .putExtra("subcategryId", List_Item.get(position).getId())
//                            .putExtra("sectionName",List_Item.get(position).getName()));
//                }else {
//                    context.startActivity(new Intent(context, ProductActivity.class)
//                            .putExtra("subCategryId", List_Item.get(position).getId())
//                            .putExtra("sectionName",List_Item.get(position).getName()));
//
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != List_Item ? List_Item.size() : 0);
    }

    protected class MenuItemViewHolder extends RecyclerView.ViewHolder {


        private TextView name;
        private CardView cardView;


        public MenuItemViewHolder(@NonNull View view) {
            super(view);
             cardView = view.findViewById(R.id.main);
            name = view.findViewById(R.id.text);


        }

    }
}
